from flask import Flask, request, flash, url_for, redirect, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///records.sqlite3'
app.config['SECRET_KEY'] = "random string"

db = SQLAlchemy(app)

class records(db.Model):
    id = db.Column('record_id', db.Integer, primary_key = True)
    company_name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    address = db.Column(db.String(100))
    phone = db.Column(db.Integer())

    def __init__(self, company_name, email, address, phone ):
        self.company_name = company_name
        self.email = email
        self.address = address
        self.phone = phone

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/submit_record')
def submit_record():
    company_name = request.args.get('company_name')
    email = request.args.get('email')
    address = request.args.get('address')
    phone = request.args.get('phone')

    new_record = records(company_name, email, address, phone)
    db.session.add(new_record)
    db.session.commit()
    flash('Record is successfully added')
    return render_template('home.html', company_name=new_record.id)

@app.route('/edit_existing_record')
def edit_existing_record():
    company_name = request.args.get('company_name')
    email = request.args.get('email')
    address = request.args.get('address')
    phone = request.args.get('phone')

    student = records.query.filter_by(company_name=company_name).first()
    student.company_name = company_name
    student.email = email
    student.address = address
    student.phone = phone

    db.session.commit()
    flash('Record successfully edited')
    return render_template('home.html')

@app.route('/edit_record/<company_name>/<phone>/<email>/<address>')
def edit_record(company_name, phone, email, address):
    return render_template('edit_record.html', company_name=company_name, phone=phone, email=email, address=address)

@app.route('/delete_record', defaults={'phone': None})
@app.route('/delete_record/<phone>')
def delete_record(phone):
    if (phone == None):
        phone = request.args.get('phone')
    del_student = records.query.filter_by(phone=phone).first()

    if (del_student == None):
        flash('Record NOT FOUND,  ENTER correct Phone number')
    else:
        db.session.delete(del_student)
        db.session.commit()
        flash('Record successfully deleted')

    return render_template('home.html')

@app.route('/show_all')
def show_all():
    return render_template('show_all.html', records = records.query.all() )

if __name__ == '__main__':
    db.create_all()
    app.run(debug = True)
